# Personal Portfolio Bernadetha - Acquiring and Analyzing Spatial Data

## Background

Delhi - India is considered as one of the most polluted city in the world with the recent surge in air pollution has reached four times higher than the recommended limit by the World Health Organization [1] As urban greenery has been proven to decrease temperature in big cities and have direct impact to the air quality [2], we aim to see whether vegetation in Delhi can help improving air quality and air temperature. In order to see the effect of vegetation to air quality and air temperature, spatial data analysis needed to be conducted to find the best samples and observation units. 

Transportation is considered as one of the most major contributor to air quality and air temperature [3]. Therefore, we took samples from areas that have the same level of vehicle quantity on the road but different availability of vegetation. Due to the emerging of electric vehicle (EV), the availability of electric charging station in the area was also taken into account as a proxy of the potential amount of EV being used in the area. It is important to be taken into account since electric vehicle doesn't emit pollution, thus have no contribution to the air quality and air temperature. Moreover, different economic level in different area in Delhi might also impact the type of vehicle that are used in the area. For example, in the area/neighborhood with high economic level might have more advance vehicle that emit less emission or EV that have no direct emission. Therefore, economic level in different areas in Delhi was also taken into account. In this project, I acquired spatial data from open source and analyzed it using the amount of vehicle on the road, availability of EV charging station, and economic level as control variables to find the best samples and observation units.

## Methodology and Data Source Used

### Data Source Used

In this analysis, the data that were used are mentioned as follows:

- Remote sensing data: [Earth Data](https://www.earthdata.nasa.gov/)
- Road traffic data as a proxy of the amount of vehicle on the road: [Tomtom](https://move.tomtom.com/)
- EV charging stations: [Switch Delhi](https://ev.delhi.gov.in/openev/data/static/)
- Property price as a proxy of economic level: [Kaggle](https://www.kaggle.com/datasets/goelyash/housing-price-dataset-of-delhiindia/data)
- Delhi weather stations: [Air Quality Historical Data Platform](https://aqicn.org/data-platform/register/) and [Delhi Pollution Control Committee](https://www.dpccairdata.com/dpccairdata/display/index.php)

### Methodology

Analysis was conducted using multivariate analysis method to create several clusters based on the similarity of the amount of vehicle on the road, availability of EV charging station, and economic level. Observation will be conducted through each available weather station in Delhi. For each weather station in each clusters, the average of NDVI that is calculated from remote sensing data is calculated as a proxy of vegetation availability. All analysis was conducted in ArcGIS pro. 

## Implementation

### Data Wrangling

Data wrangling process was conducted by my colleague for remote sensing data, road traffic data. EV charging station, and property price data. As a proxy for vegetation, we use Normalized Different Vegetation Index (NDVI) that are calculated using R. Car flow was calculated from road traffic data and scored based on the traffic density in each post code in Delhi. The traffic was classified into 5 categories (1 = lowest traffic, 5 = highest traffic). For the property price data, the average of property price per post code was calculated, resulting in polygon of Delhi that contains property price per post code. Moreover, the total of EV charging station per post code was also calculated, resulting in polygon of Delhi that contains the sum of the EV charging station for each post code. In general, the data wrangling process resulted in several data that are ready to be analyzed, as follows:

1. Traffic score per post code
2. Average property price per post code
3. Total EV station per post code
4. Delhi NDVI

### Creating Cluster Using Multivariate Analysis

First, traffic score per post code, average property price per post code, and total EV station per postcode data were combined using Union Geoprocessing tool. By combining three polygon data using Union, one polygon with all the attributes from the three data was created. After all three data was combined, multivariate analysis was conducted based on similarities in traffic score, average property price, and total EV station. From multivariate analysis, seven cluster was formed using K-means method with optimized seed location initialization method. Seven cluster was chosen based on elbow curve law. 

![image](Model_1.jpg)

### Inserting The Weather Stations to Delhi Post Code Boundary

After retrieving the weather station location data, the weather station points were overlaid on top of the cluster polygon. However, there are several stations that are located outside the Delhi area. Therefore, Clip Geoprocessing tool are applied to eliminate the stations that are outside the Delhi area. Apparently, there are several weather stations in Delhi area that only have either air quality data or air temperature data. Since we wanted to analyse both air quality and air temperature data, therefore select tool was used to select eliminate the weather stations that only have one of the required data. 

![image](Model_2.jpg)

### Creating Buffer and Calculating Mean NDVI for Each Weather Station

Each weather station in each cluster is the observation unit of the research. Generally, the sensor range for a weather station is 3 km, meaning the air quality and air temperature data that will be analysed fall within 3 km range from each weather station. Therefore, a buffer with 3 km radius was created for each weather station. Since we wanted to see the effect of vegetation to the air quality and air temperature, the average NDVI within 3 km radius for each weather station was calculated using zonal statistic tool.

![image](Model_3.jpg)

## Results

From multivariate analysis, a polygon consist of 7 clusters was produced, as follow:

![image](Delhi_clustering_map.jpg)

There are two clusters that relatively very small compared to other clusters. Therefore, those two clusters are eliminated so the observation was only conducted in five clusters as five clusters still fulfill the elbow curve law for the given dataset.

After the weather stations with average NDVI was added to the clusters, the result shows as follow:

![image](Cluster_MeanNDVI_map.jpg)

The darker the green means there are more vegetation in the particular area.

From the multivariate and mean NDVI analysis, the samples and observation units are defined. For each cluster, different weather stations with different NDVI were compared and correlation analysis between NDVI and air quality and NDVI and air temperature are conducted.

## Conclusion

In conclusion, there are seven clusters derived from multivariate analysis. The clusters with similarity in traffic score, average property price, and total EV charging station are used as samples. Weather stations in each clusters with different NDVI was chosen as a observation unit.

As acquiring spatial data from open source and anaylze it are two of my learning goals, being able to download a spatial data from nasa earth data website and analyze it using ArcGIS pro shows that I have accomplished my learning goals. In the analysis process, I also use chatgpt and help from my colleague to figure out what kind of tools that I need to use in ArcGIS pro to analyze the data. Deeper analysis in ArcGIS pro to transform the raw data into a dataset that are ready to analyse is the part that I still need to learn and develop further.

## Reference

[1] Ellis-Petersen, H. (2023). Delhi air pollution spikes to 100 times WHO health limit. the Guardian. https://www.theguardian.com/world/2023/nov/03/delhi-india-air-quality-pollution-spike-world-health-organization-limit 

[2] Aram, F., García, E. H., Solgi, E., & Mansournia, S. (2019). Urban green space cooling effect in cities. Heliyon, 5(4), e01339. https://doi.org/10.1016/j.heliyon.2019.e01339

[3] Ali, S., & Sharma, B. (Producer). (2023, October 11). Delhi's Air Quality Needs Data-Driven Action. WRI India. Retrieved from https://wri-india.org/blog/delhis-air-quality-needs-data-driven-action 


```python

```
