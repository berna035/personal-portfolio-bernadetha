# Personal Portfolio Bernadetha - Basic Data Analysis



## Background

Data analysis is an important step in any research. A thorough data analysis can result in robust conclusion that can help solving real-life problems. In my group project, my team and I tried to analyse the impact of vegetation on air quality index and air temperature in Delhi, India. Delhi is one of the most poluted city in the world with the recent surge in air pollution has reached four times higher than the recommended limit by the World Health Organization [1] . As urban greenery has been proven to decrease temperature in big cities and have direct impact to the air quality [2], we aim to see whether vegetation in Delhi can help improving air quality and air temperature. Normalized Difference Vegetation Index (NDVI) was used as a proxy of the availability of vegetation or urban green area, while air quality was shown by air quality index (AQI) and air temperature was shown by air temperture (in degree celcius) from weather stations available in the city. In analysing data, I used statistical analysis to see whether there is a correlation between NDVI and AQI, and NDVI and air temperature.


## Methodology and Data Source Used

### Data Source Used

Air quality parameter, air temperature, and NDVI data from June 2023 were analysed to represent the correlation between NDVI and AQI, and NDVI and air temperature. All the data were retrieved from sources that are available online as follows:

- Air quality parameter: [Air Quality Historical Data Platform](https://aqicn.org/data-platform/register/)
- Air temperature: [Delhi Pollution Control Committee](https://www.dpccairdata.com/dpccairdata/display/index.php)
- NDVI: [Earth Data](https://www.earthdata.nasa.gov/)

### Methodology

#### Data Collection
All data from sources above were collected directly from the website

#### Data Preparation
1. Air quality parameters are calculated to get the air quality index (AQI). Calculation was conducted in excel by my colleague
2. Missing data from air temperature was filled by interpolation of available data. Calcuation was conducted in excel by my colleague
3. Average of NDVI within 3 km radius from each weather station was calculated to get the mean NDVI for each station. Calculation was conducted in ArcGIS pro

#### Statistical Analysis

1. Normality test was conducted using saphiro to check the distribution of data
2. Analysis of Variance was conducted to check the variance between samples (AQI and air temperature) collected from different weather stations
3. Correlation analysis was conducted to see the correlation and its characteristic between NDVI and AQI, and NDVI and air temperature


## Implementation

Normality test was conducted using saphiro with following code:

```
Weather1 = df1['AQI']
sns.histplot(Weather1)
plt.show()

statistic, p_value = shapiro(df1['AQI'])

alpha = 0.05
if p_value > alpha:
    print("The dataset follows a normal distribution.")
else:
    print("The dataset does not follow a normal distribution.")
```

From the normality test, most of the data was not normally distributed. Therefore, non-parametric analysis method was chosen for both variance analysis and correlation analysis. Analysis was conducted in three cluster that have already determined based on the similarity of the traffic density, economic level, and the availability of electric vehicle charging station as a proxy of the type of vehicle that is available in the traffic.

Analysis of variance was conducted using Kruskal-Wallis test, a non-parametric approach to the one-way ANOVA. Kruskal-Wallis test compares three or more groups on a dependent variables to see whether the samples originate from the same distribution [3]. From the Kruskal-Wallis test, all the samples for AQI and air temperature are have a significance difference. Therefore, correlation test was conducted to see whether there's a correlation between the dependent and independent variables.

Correlation test was conducted using Spearman's rank correlaction, a non-parametric analysis method for correlation [4]. In this analysis, I conducted hypothesis testing given the initial and alternative hypothesis as follows:
H0: There is no correlation between independent variables (NDVI) and dependent variables (AQI or temperature)
H1: There is a correlation between independent variables (NDVI) and dependent variables (AQI or temperature)
When p-value is lower than alpha (0.05) it means the initial hypothesis is rejected. Otherwise, the initial hypothesis is accepted


Note: Complete script can be found in the files above


## Results

From the correlation analysis, the results shows inconcistency between clusters. One out of four clusters has significance correlation between NDVI and temperature, and two out of four clusters have significance correlation between NDVI and AQI. Inconsistency in result between clusters might caused by the small datasets that are being used for analysis [5]. 

![image](NDVI_Temperature.jpg)

![image](NDVI_AQI.jpg)

In this analysis, I only used datasets from one month period. Longer period of analysis might lead to a more consistent result. Moreover, correlation between vegetation and AQI, and vegetation and air temperature is proven by recent literatures by also taking into account other factor that contribute to the air quality and air temperature [6]. The lack of other control variables might be the cause of a different result from this analysis. 


## Conclusion

From the analysis conducted, the correlation between vegetation and air quality and temperature can not be proven. However, this result appears to be an opposite from other current literature that shows a correlation between vegetation and both air quality and temperature. Therefore, further analysis needs to be conducted on a bigger datasets while also considering other factors that might affect air quality and air temperature.

As data analysis using python is one of my learning goals, being able to conduct a statistical analysis using python shows that I have accomplished my learning goals. Further improvement on how to use python for other analysis method is indeed required for my personal development.


## Reference

[1] Ellis-Petersen, H. (2023). Delhi air pollution spikes to 100 times WHO health limit. the Guardian. https://www.theguardian.com/world/2023/nov/03/delhi-india-air-quality-pollution-spike-world-health-organization-limit 

[2] Aram, F., García, E. H., Solgi, E., & Mansournia, S. (2019). Urban green space cooling effect in cities. Heliyon, 5(4), e01339. https://doi.org/10.1016/j.heliyon.2019.e01339

[3] Lalane, C., Mesbah, M. (2016). 2-Measures of Association, Comparisons of Means and Proportions for Two Samples or More. Biostatistics and Computer-based Analysis of Health Data using Stata. https://doi.org/10.1016/B978-1-78548-142-0.50002-X

[4] Parab, S., Bhalerao, S. (2010). Choosing Statistical Test. International Journal of Ayurveda Research 1(3): 187-191. https://doi.org/10.4103%2F0974-7788.72494

[5] Ai, H., Zhang, X., & Zhou, Z. (2023). The impact of greenspace on air pollution: Empirical evidence from China. Ecological Indicators, 146, 109881. https://doi.org/10.1016/j.ecolind.2023.109881  

[6] Kandelan, S. N., Yeganeh, M., Peyman, S., Panchabikesan, K., & Eicker, U. (2022). Environmental study on greenery planning scenarios to improve the air quality in urban canyons. Sustainable Cities and Society, 83, 103993. https://doi.org/10.1016/j.scs.2022.103993


```python

```
