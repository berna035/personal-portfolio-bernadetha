# Personal Portfolio-Bernadetha Maria Hastuti

Throughout the Data Science for Smart Environments courses, I gained a lot of new knowledge in data analysis, as well as its societal and ethical issue. Especially through my group project, I developed new skills on spatial data analysis and statistical analysis using python. In this portfolio, you can find my learning result based on my initial learning goals. You can access each result as follows:

1. My initial personal learning goals: [Introduction](https://git.wur.nl/berna035/personal-portfolio-bernadetha/-/blob/main/Introduction.ipynb)
2. Learning goal 1 & 2: [Spatial Data Analysis](https://git.wur.nl/berna035/personal-portfolio-bernadetha/-/tree/main/Spatial%20Data%20Analysis)
3. Learning goal 3: [Statistical Analysis in Python](https://git.wur.nl/berna035/personal-portfolio-bernadetha/-/tree/main/Statistical%20Analysis%20in%20Python)
4. Learning goal 4: [Social_And_Ethical_Aspect](https://git.wur.nl/berna035/personal-portfolio-bernadetha/-/blob/main/Social_and_Ethical_Aspect.ipynb)
5. My reflection on my learning process: [Reflection](https://git.wur.nl/berna035/personal-portfolio-bernadetha/-/blob/main/Reflection.ipynb)


